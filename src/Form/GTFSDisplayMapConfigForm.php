<?php

namespace Drupal\gtfs_display_map\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 */
class GTFSDisplayMapConfigForm extends ConfigFormBase {
  const SETTINGS = 'gtfs_display_map.settings';

  /**
   *
   */
  public function getFormId() {
    return 'gtfs_display_map_settings';
  }

  /**
   *
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['mapbox_access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mapbox Access Token'),
      '#default_value' => $config->get('mapbox_access_token'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('mapbox_access_token', $form_state->getValue('mapbox_access_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
