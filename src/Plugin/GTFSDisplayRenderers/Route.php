<?php

namespace Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers;

use Drupal\gtfs\Entity\GTFSEntityBase;

class Route extends Base {
  public static function build(GTFSEntityBase $route, &$build = []) {
    $build = parent::build($route, $build);
    $build['#attached']['library'][] = 'gtfs_display_map/route';

    $agency = $route->agency()->toGTFSObject();
    $build['#attached']['drupalSettings']['gtfs_display_map']['agency'] = array_merge_recursive(
      $build['#attached']['drupalSettings']['gtfs_display_map']['agency'] ?? [],
      $agency
    );

    $routeGTFS = $route->toGTFSObject() + [
      'geojson' => $route->geojson(),
      'agency' => $route->agency()->toGTFSObject()
    ];
    $build['#attached']['drupalSettings']['gtfs_display_map']['route'] = array_merge_recursive(
      $build['#attached']['drupalSettings']['gtfs_display_map']['route'] ?? [],
      $routeGTFS
    );

    $stops = array_values(array_map(function ($stop) {
      return $stop->toGTFSObject() + [
        'geojson' => $stop->geojson(),
        'routes' => array_values(array_map(function ($route) {
          return $route->toGTFSObject();
        }, $stop->routes()))
      ];
    }, $route->stops()));
    $build['#attached']['drupalSettings']['gtfs_display_map']['stops'] = array_merge_recursive(
      $build['#attached']['drupalSettings']['gtfs_display_map']['stops'] ?? [],
      $stops
    );
    $build['#markup'] = '<div id="map"></div>';
    return $build;
  }

}