<?php

namespace Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers;

use Drupal\gtfs\Entity\GTFSEntityBase;

class Trip extends Base {
  public static function build(GTFSEntityBase $trip, &$build = []) {
    $build = parent::build($trip, $build);
    $build['#attached']['library'][] = 'gtfs_display_map/trip';
    $build['#attached']['drupalSettings']['gtfs_display_map']['trip'] = $trip->toGTFSObject() + [
        'geojson' => $trip->geojson(),
      ];
    $build['#attached']['drupalSettings']['gtfs_display_map']['stops'] = array_values(array_map(function ($stop) {
      return $stop->toGTFSObject() + ['geojson' => $stop->geojson()];
    }, $trip->stops()));
    $build['#markup'] = '<div id="map"></div>';
    return $build;
  }

}