<?php

namespace Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers;

use Drupal\gtfs\Entity\GTFSEntityBase;

class Agency extends Base {
  public static function build(GTFSEntityBase $agency, &$build = []) {
    module_load_include('module', 'gtfs_geo', 'gtfs_geo');
    $build = parent::build($agency, $build);
    $build['#attached']['library'][] = 'gtfs_display_map/agency';
    $build['#attached']['drupalSettings']['gtfs_display_map']['agency'] = $agency->toGTFSObject();
    $build['#attached']['drupalSettings']['gtfs_display_map']['routes'] = array_values(array_map(function ($route) {
      return $route->toGTFSObject() + ['geojson' => $route->geojson()];
    }, $agency->routes()));
//    $build['#attached']['drupalSettings']['gtfs_display_map']['route_overlaps'] = \gtfs_geo_calculate_route_overlap(array_map(function ($route) { return $route->id(); }, $agency->routes()));
    $build['#markup'] = '<div id="map"></div>';
    return $build;
  }

}