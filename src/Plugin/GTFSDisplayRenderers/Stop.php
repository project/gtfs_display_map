<?php

namespace Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers;

use Drupal\gtfs\Entity\GTFSEntityBase;

class Stop extends Base {
  public static function build(GTFSEntityBase $stop, &$build = []) {
    module_load_include('module', 'gtfs_geo', 'gtfs_geo');
    $build = parent::build($stop, $build);
    $build['#attached']['library'][] = 'gtfs_display_map/stop';
    $stopGTFS = $stop->toGTFSObject() + [
      'geojson' => $stop->geojson(),
      'routes' => array_values(array_map(function ($route) {
        return $route->toGTFSObject();
      }, $stop->routes()))
    ];
    $build['#attached']['drupalSettings']['gtfs_display_map']['stop'] = array_merge_recursive(
      $build['#attached']['drupalSettings']['gtfs_display_map']['stop'] ?? [],
      $stopGTFS
    );
    $routes = array_values(array_map(function ($route) {
      return $route->toGTFSObject() + [
          'geojson' => $route->geojson(),
          'agency' => $route->agency()->toGTFSObject()
        ];
    }, $stop->routes()));
    $build['#attached']['drupalSettings']['gtfs_display_map']['routes'] = array_merge_recursive(
      $build['#attached']['drupalSettings']['gtfs_display_map']['routes'] ?? [],
      $routes
    );
//    $build['#attached']['drupalSettings']['gtfs_display_map']['route_overlaps'] = \gtfs_geo_calculate_route_overlap(array_map(function ($route) { return $route->id(); }, $stop->routes()));
    $build['#markup'] = '<div id="map"></div>';
    return $build;
  }

}