<?php

namespace Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers;

use Drupal\gtfs\Entity\GTFSEntityBase;
use Drupal\gtfs_display_map\Form\GTFSDisplayMapConfigForm;

class Base {
  public static function build(GTFSEntityBase $instance, &$build = []) {
    $build['#markup'] = 'No custom map registered for ' . static::class;
    $build['#attached']['library'][] = 'gtfs_display_map/base';
    $build['#attached']['drupalSettings']['gtfs_display_map']['mapboxToken'] = \Drupal::config(GTFSDisplayMapConfigForm::SETTINGS)->get('mapbox_access_token');
    return $build;
  }
}