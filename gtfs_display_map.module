<?php
module_load_include('module', 'gtfs_geo', 'gtfs_geo'); // Y tho

use Drupal\gtfs\Entity\Agency as GTFSAgency;
use Drupal\gtfs\Entity\Route as GTFSRoute;
use Drupal\gtfs\Entity\Stop as GTFSStop;
use Drupal\gtfs\Entity\Trip as GTFSTrip;
use Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers\Agency;
use Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers\Trip;
use Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers\Stop;
use Drupal\gtfs_display_map\Plugin\GTFSDisplayRenderers\Route;
use Brick\Geo\Engine\GeometryEngineRegistry;
use Brick\Geo\Engine\PDOEngine;
use Brick\Geo\Point;
use Brick\Geo\IO\GeoJSONReader;
use Brick\Geo\IO\GeoJSONWriter;
use Brick\Geo\MultiLineString;
use Brick\Geo\GeometryCollection;
use Brick\Geo\IO\GeoJSON\FeatureCollection;

function gtfs_display_map_gtfs_display_agency_map_render_alter(&$build, GTFSAgency $agency) {
  $build = Agency::build($agency, $build);
}

function gtfs_display_map_gtfs_display_route_map_render_alter(&$build, GTFSRoute $route) {
  $build = Route::build($route, $build);
}

function gtfs_display_map_gtfs_display_trip_map_render_alter(&$build, GTFSTrip $trip) {
  $build = Trip::build($trip, $build);
}

function gtfs_display_map_gtfs_display_stop_map_render_alter(&$build, GTFSStop $stop) {
  $build = Stop::build($stop, $build);
}

function gtfs_display_map_gtfs_display_stop_default_render_alter(&$build, GTFSStop $stop) {
  $build['container']['map'] = [
    '#weight' => 3,
    '#type' => 'inline_template',
    '#template' => '<iframe style="border: none; width: 100%; height: 100vh" src="{{ url }}"></iframe>',
    '#context' => [
      'url' => $stop->links()['map']
    ],
  ];
}

// A helper for the alters
function gtfs_display_map_derive_map_link($links) {
  return str_replace('api/v1', 'display', $links['self']) . '/map';
}

function gtfs_display_gtfs_agency_links_alter(&$links, $agency) {
  $links['map'] = gtfs_display_map_derive_map_link($links);
}

function gtfs_display_gtfs_route_links_alter(&$links, $route) {
  $links['map'] = gtfs_display_map_derive_map_link($links);
}

function gtfs_display_gtfs_trip_links_alter(&$links, $trip) {
  $links['map'] = gtfs_display_map_derive_map_link($links);
}

function gtfs_display_gtfs_stop_links_alter(&$links, $stop) {
  $links['map'] = gtfs_display_map_derive_map_link($links);
}

function gtfs_display_map_generate_overlap($route_1_id, &$context) {$route_1 = GTFSRoute::load($route_1_id);

  $shape_ids = \Drupal::database()->query(
    'SELECT `shape_id`
          FROM {gtfs_shape_field_data}
          WHERE `id` IN (
            SELECT `shape_id`
            FROM {gtfs_trip_field_data}
            WHERE `route_id` = :route_id
        )',
    [':route_id' => $route_1_id]
  )->fetchAll(\PDO::FETCH_COLUMN);

  $coordinates = \Drupal::database()->query(
    'SELECT `shape_pt_lon`,`shape_pt_lat`
            FROM {gtfs_shape_field_data}
            WHERE `shape_id` IN :shape_ids[]',
    [':shape_ids[]' => $shape_ids]
  )->fetchAll(\PDO::FETCH_NUM);
}

function gtfs_display_map_generate_overlap_old($route_1_id, &$context) {
  geophp_load();

  $route_1 = GTFSRoute::load($route_1_id);

  $geojson_1 = $route_1->geojson();
  $shape_1 = geoPHP::load(json_encode($geojson_1), 'json');

  $routes = $route_1->agency()->routes();

  foreach ($routes as $route_2) {
    $geojson_2 = $route_2->geojson();
    $shape_2 = geoPHP::load(json_encode($geojson_2), 'json');

    if (!$shape_1->touches($shape_2)) continue;

    fprint("hi", 'log');

    $overlap = $shape_1->intersection($shape_2);

    \Drupal::database()->insert('gtfs_overlap')->fields([
      'route_1' => $route_1->id(),
      'route_2' => $route_2->id(),
      'data' => $overlap->out('json')
    ])->execute();
  }

  $context['message'] = 'Generated overlaps for ' . $route_1->label();
}

function gtfs_display_map_generate_overlaps() {
  $routes = GTFSRoute::loadMultiple();

  $batch = [
    'title' => 'Generating Overlaps',
    'operations' => array_map(function ($route) {
      return [
        'gtfs_display_map_generate_overlap',
        [$route->id()]
      ];
    }, $routes)
  ];

  batch_set($batch);
}

function gtfs_display_map_route_intersection($routes) {
  $db = \Drupal::database()->getConnectionOptions();
  $pdo = new PDO("mysql:host={$db['host']}", $db['username'], $db['password']);
  GeometryEngineRegistry::set(new PDOEngine($pdo));
  $reader = new GeoJSONReader();
  $writer = new GeoJSONWriter();

  $return = [];

  foreach ($routes as $route) {
    $geojson = $route->geojson();
    $shape = $reader->read(json_encode($geojson));
    $srid = 4326;
    $overlaps = [];

    foreach ($routes as $route_compare) {
      if ($route->id() === $route_compare->id()) continue;
      $geojson_compare = $route_compare->geojson();
      $shape_compare = $reader->read(json_encode($geojson_compare));
      if (!$shape->overlaps($shape_compare->withSRID($srid))) {
        continue;
      }
      $overlaps[$route_compare->get('route_id')->value] = $shape->intersection($shape_compare);
    }

    foreach ($overlaps as $id => &$overlap) {

      foreach ($overlaps as $id_compare => &$overlap_compare) {
        if ($id === $id_compare) continue;
        if (!$overlap->withSRID($srid)->overlaps($overlap_compare->withSRID($srid))) continue;
        $overlaps[implode('|', [$id, $id_compare])] = $overlap->intersection($overlap_compare->withSRID($srid));
        $overlap = $overlap->withSRID($srid)->difference($overlap_compare->withSRID($srid));
      }

    }

    foreach ($overlaps as $ids => $overlap) {
      $ids = explode(',', $ids);
      $json = json_decode($writer->write($overlap));
      $json->properties = [
        'overlap' => [ $route->get('route_id')->value ] + $ids
      ];
      $return[] = $json;
      $shape = $shape->withSRID($srid)->difference($overlap->withSRID($srid));
    }

    $json = json_decode($writer->write($shape));
    $json->properties = ['route' => $route->get('route_id')->value];
    $return[] = $json;
  }


  // TODO filter all the points and multipoints
  return $return;
}

