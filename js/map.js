function initializeMap(parameters = {}) {
  const defaultParameters = {
    minZoom: 1,
    maxZoom: 22
  };
  const initialParameters = Object.assign({}, defaultParameters, parameters);
  const { mapboxToken } = drupalSettings.gtfs_display_map;
  mapboxgl.accessToken = mapboxToken;
  const map = window.map = new mapboxgl.Map({
    container: 'map', // container ID
    style: 'mapbox://styles/tylershuster/cknkugmyn0wh517pdkxz87x1u', // style URL
    center: [-122.432785,37.663304], // starting position [lng, lat]
    zoom: 10, // starting zoom
  });
  map.on('load', () => {
    map.addSource('empty', {
      type: 'geojson',
      data: { type: 'FeatureCollection', features: [] }
    });
    const zIndex3 = map.addLayer({
      id: 'z-index-3',
      type: 'symbol',
      source: 'empty'
    });

    const zIndex2 = map.addLayer({
      id: 'z-index-2',
      type: 'symbol',
      source: 'empty'
    }, 'z-index-3');

    const zIndex1 = map.addLayer({
      id: 'z-index-1',
      type: 'symbol',
      source: 'empty'
    }, 'z-index-2'); // place this layer below zIndex2
  })
  return map;
}
