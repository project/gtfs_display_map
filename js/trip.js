jQuery(document).ready(function ($) {
  const map = window.map = initializeMap();
  const { trip, stops } = drupalSettings.gtfs_display_map;
  const shape = L.geoJSON(trip.geojson);
  shape.addTo(map);
  stops.forEach(function (stop) {
    L.geoJSON(stop.geojson)
        .bindPopup(`<a href='${displayLink(stop)}'>View Stop</a>`)
        .addTo(map);
  });
  map.fitBounds(shape.getBounds());

});