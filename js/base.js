console.info(`Data for this page is available at %c${window.location.href.replace('display', 'api/v1')}`, `display: block;color: white;text-decoration: underline`);

function displayLink(entity) {
  const query = getQuery();

  // TODO: abstract this to use the code that generates these URLs in REST, etc
  if (entity.stop_code && query.pattern_stop && query.pattern_stop.includes('{stop_code}')) {
    return query.pattern_stop.replace('{stop_code}', entity.stop_code);
  } else if (entity.stop_id && query.pattern_stop) {
    return query.pattern_stop.replace('{stop_id}', entity.stop_id);
  } else if (entity.route_id && query.pattern_route) {
    return query.pattern_route.replace('{agency_id}', entity.agency_id).replace('{route_id}', entity.route_id);
  }

  return entity.self.replace('api/v1', 'display') + '/map';
}

const svgUrl = (svgStr, width = 10, height = 10) => {
  svgStr = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ${width} ${height}">${svgStr}</svg>`;

  const encoded = encodeURIComponent(svgStr)
      .replace(/'/g, '%27')
      .replace(/"/g, '%22');

  const header = 'data:image/svg+xml,';

  return header + encoded;
}

// const vehicleIcon = L.icon({
//   iconUrl: `/gtfs/display/icon/bus?color=steelblue`,
//   iconSize: [22, 22],
//   iconAnchor: [11, 11]
// });
//
// const stopIcon = L.icon({
//   iconUrl: `/gtfs/display/icon/circle?color=chocolate`,
//   iconSize: [11, 11],
//   iconAnchor: [5.5, 5.5]
// });

const addHex = (string) => `#${string.replaceAll('#', '')}`;

const routeColor = (route) => route.route_color ? addHex(route.route_color) : randomColor({ seed: route.route_id });

const markerFromStop = (stop) => {
  const el = document.createElement('div');
  el.id = stop.stop_id;
  el.className = 'stop-marker';
  // const annotation = document.createElement('span');
  // annotation.className = 'annotation';
  // annotation.innerText = stop.stop_id;
  // el.appendChild(annotation);
  const wrapper = document.createElement('div');
  wrapper.className = 'route-wrapper';
  el.appendChild(wrapper);
  stop.routes.forEach(route => {
    const span = document.createElement('span');
    span.style.backgroundColor = routeColor(route);
    span.dataset.routeId = route.route_id;
    wrapper.appendChild(span);
  });
  const marker = new mapboxgl.Marker(el);
  marker.setLngLat([stop.stop_lon, stop.stop_lat]);
  const link = `<a href="${displayLink(stop)}">${stop.stop_name || `Stop ${stop.stop_id}`}</a>`;
  const content = `<h2>${link}</h2><span>Routes Serviced</span><br />${stop.routes.map(route => {
    return `<a href="${displayLink(route)}" class="route-link"><span class="route-color-bar" style="color: ${routeColor(route)}">|</span>${route.route_long_name}</a>`;
  }).join('<br />')}`
  const popup = new mapboxgl.Popup({
    closeOnClick: false,
    focusAfterOpen: false,
    className: 'stop-popup',
    maxWidth: '30rem'
  }).setHTML(content);
  marker.setPopup(popup);
  return marker;
}
const addStopToMap = (stop) => {
  const marker = markerFromStop(stop);
  marker.addTo(window.map);
  return marker;
}

const addRouteToMap = (route) => {
  return addSegmentToMap(route.route_id, route.geojson, [route]);
}

const lineWeight = 5;
const strokeWidth = 1;
const bgWidth = 3;

const addSegmentToMap = (id, feature, routes) => {
  const segmentWidth = (routes.length * lineWeight);
  map.addSource(id, {
    type: 'geojson',
    data: feature
  });
  map.addLayer({
    id: `${id}-outline`,
    type: 'line',
    source: id,
    layout: {
      'line-cap': 'round',
      'line-join': 'round'
    },
    paint: {
      'line-color': '#000',
      'line-width': segmentWidth + (bgWidth * 2) + (strokeWidth * 2)
    }
  }, 'z-index-1');
  map.addLayer({
    id: `${id}-bg`,
    type: 'line',
    source: id,
    layout: {
      'line-cap': 'round',
      'line-join': 'round'
    },
    paint: {
      'line-color': '#fff',
      'line-width': segmentWidth + (bgWidth * 2)
    }
  }, 'z-index-2');
  routes.forEach((route, index) => {
    map.addLayer({
      id: `${id}-${route.route_id}`,
      type: 'line',
      source: id,
      layout: {
        'line-cap': 'round',
        'line-join': 'round'
      },
      paint: {
        'line-color': routeColor(route),
        'line-width': lineWeight,
        'line-offset': (index) * (lineWeight + 1) - (segmentWidth / 2) + ((lineWeight + 1) / 2)
      }
    }, 'z-index-3');
  });
}

const addStopsToMap = (stops) => {
  map.addSource('stops', {
    type: 'geojson',
    data: {
      type: 'FeatureCollection',
      features: stops.map(stop => {
        return {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [stop.stop_lon, stop.stop_lat]
          },
          properties: {
            stop_id: stop.stop_id,
            routes: JSON.stringify(stop.routes.map(route => ({
              route_id: route.route_id,
              route_color: route.route_color
            })))
          }
        }
      })
    },
    cluster: true,
    clusterRadius: 50,
    clusterMaxZoom: 14,
    clusterProperties: {
      routes: ['concat', ['get', 'routes']]
    }
  });

  map.addLayer({
    id: 'stops',
    type: 'circle',
    source: 'stops',
    filter: ['!=', 'cluster', true],
  });

  window.markers = {};
  window.markersOnScreen = {};

  function updateMarkers() {
    var newMarkers = {};
    var features = map.querySourceFeatures('stops');

    features.forEach((feature, index) => {
      var coords = feature.geometry.coordinates;
      var props = feature.properties;
      const stop = window.stops[props.stop_id];

      if (props.cluster) {
        var id = props.cluster_id;
        var marker = markers[id];

        if (!marker) {
          const el = document.createElement('div');
          el.className = 'stop-marker';
          const pointCount = document.createElement('span');
          pointCount.className = 'annotation';
          pointCount.innerText = props.point_count + ' Stops';
          el.appendChild(pointCount);
          const wrapper = document.createElement('div');
          wrapper.className = 'route-wrapper';
          el.appendChild(wrapper);
          const routes = props.routes.replaceAll('][', 'SPLARBEND][SPLARBSTART').split('][').map(s => s.replaceAll('SPLARBEND', ']').replaceAll('SPLARBSTART', '[')).map(s => JSON.parse(s)).flat();
          const routesIncluded = [];
          routes.forEach(route => {
            if (routesIncluded.includes(route.route_id)) return;
            routesIncluded.push(route.route_id);
            const span = document.createElement('span');
            span.style.backgroundColor = routeColor(route);
            span.dataset.routeId = route.route_id;
            wrapper.appendChild(span);
          });
          marker = markers[id] = new mapboxgl.Marker({
            element: el
          }).setLngLat(coords);
        }

        newMarkers[id] = marker;

        if (!markersOnScreen[id]) marker.addTo(map);
      } else {
        const id = props.stop_id;
        let marker = window.markers[id];

        if (!marker) {
          marker = markers[id] = markerFromStop(stop);
        }

        newMarkers[id] = marker;

        if (!markersOnScreen[id]) marker.addTo(map);
      }
    });

    Object.keys(window.markers).forEach(id => {
      if (!newMarkers[id]) {
        window.markers[id].remove();
        delete window.markers[id];
      }
    });

    markersOnScreen = newMarkers;
  }

  map.on('render', function () {
    if (!map.isSourceLoaded('stops')) return;
    updateMarkers();
  });
}

function createLegend(routes) {
  const legend = document.createElement('div');
  legend.id = 'legend';
  routes.forEach(route => {
    const figure = document.createElement('figure');
    const icon = document.createElement('i');
    icon.style.backgroundColor = routeColor(route);
    figure.appendChild(icon);
    const text = document.createElement('figcaption');
    const link = document.createElement('a');
    link.href = displayLink(route);
    link.innerText = route.route_long_name;
    text.appendChild(link);
    figure.appendChild(text);
    legend.appendChild(figure);
  });
  document.getElementById('map').insertAdjacentElement('afterend', legend);
}

// function renderEntityGeoJSON(entity, type) {
//   if (!entity.geojson || !entity.geojson.coordinates.length) return;
//
//   const geoJSONOptionsByType = {
//     route: {
//       style: function (feature) {
//         return {
//           color: routeColor(entity),
//           weight: 5
//         };
//       }
//     },
//     stop: {
//       pointToLayer: function (feature, latLng) {
//         return L.marker(latLng, { icon: stopIcon });
//       }
//     },
//     default: {}
//   };
//
//   const json = L.geoJSON(
//       entity.geojson,
//       geoJSONOptionsByType[type] || geoJSONOptionsByType.default
//   );
//
//
//
//   return json;
// }

const routeById = (id) => drupalSettings.gtfs_display_map.routes.filter(route => route.route_id === id)[0];
//
// const addOverlapsToMap = (route_overlaps) => {
//   const outlines = L.layerGroup();
//   const lineBg = L.layerGroup();
//   const busLines = L.layerGroup();
//   const lineWeight = 5;
//   const outlineColor = '#000';
//   const bgColor = '#fff';
//
//   const segment = (coords, lines) => {
//     coords = L.GeoJSON.coordsToLatLngs(coords, 0);
//
//     const segmentWidth = (lines.length * lineWeight) + 1;
//     console.log('seg', segmentWidth);
//     L.polyline(coords, {
//       color: outlineColor,
//       weight: segmentWidth + 10,
//       opacity: 1
//     }).addTo(outlines);
//     L.polyline(coords, {
//       color: bgColor,
//       weight: segmentWidth + 8,
//       opacity: 1
//     }).addTo(lineBg);
//     lines.forEach((line, index) => {
//       const shape = L.polyline(coords, {
//         color: randomColor({ seed: line.route_id, luminosity: 'dark' }),
//         weight: lineWeight,
//         opacity: 1,
//         offset: index * (lineWeight + 1) - (segmentWidth / 2) + ((lineWeight + 1) / 2)
//       }).addTo(busLines);
//       shape.bindPopup(`<a href='${displayLink(line)}'>View ${line.route_long_name}</a>`);
//       // shape.bindPopup(`${line.route_id}`);
//     });
//   }
//
//   Object.keys(route_overlaps).filter(key => key !== 'overlaps').forEach(routeId => {
//     const entity = routeById(routeId);
//     route_overlaps[routeId].coordinates.forEach(coords => {
//
//       segment(coords, [entity]);
//     })
//     return;
//     L.geoJSON(
//         route_overlaps[routeId],
//         {
//           style: function (feature) {
//             return {
//               color: entity.route_color ? addHex(entity.route_color) : randomColor({ seed: entity.route_id, luminosity: 'dark' }),
//               weight: 5
//             };
//           }
//         }
//     ).addTo(map);
//   });
//
//   const overlap = route_overlaps.overlaps;
//
//
//   overlap.geometries.forEach(feature => {
//     const overlaps = feature.properties.overlap;
//     const linesOnSegment = overlaps.map(routeById);
//
//     const renderSegment = (feature) => {
//       if (feature.type === 'GeometryCollection') {
//         feature.geometries.forEach(renderSegment);
//       } else if (feature.type === 'LineString') {
//         segment(feature.coordinates, linesOnSegment);
//       } else if (feature.type === 'MultiPoint') {
//         return;
//         segment(feature.coordinates, linesOnSegment);
//       } else if (feature.coordinates) {
//         feature.coordinates.forEach(coords => {
//           segment(coords, linesOnSegment);
//         });
//       } else {
//         feature.geometry.coordinates.forEach(coords => {
//           segment(coords, linesOnSegment);
//         });
//       }
//     };
//
//     renderSegment(feature);
//
//   });
//   outlines.addTo(map);
//   lineBg.addTo(map);
//   busLines.addTo(map);
//
//
// }