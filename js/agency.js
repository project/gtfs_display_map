jQuery(document).ready(function ($) {
  const map = window.map = initializeMap({
    zoom: 10
  });
  const { agency, routes, route_overlaps } = drupalSettings.gtfs_display_map;

  const bounds = L.latLngBounds();

  const wrapLineString = coordinates => ({
    type: 'LineString',
    coordinates
  });

  // addOverlapsToMap(route_overlaps);

  routes.forEach(function (route) {
    const shape = renderEntityGeoJSON(route, 'route');
    if (!shape) return;
    shape.bindPopup(`<a href='${displayLink(route)}'>View ${route.route_long_name}</a>`);
    bounds.extend(shape.getBounds());
    shape.addTo(map);
  });


  map.fitBounds(bounds, {
    maxZoom: 18
  });

});