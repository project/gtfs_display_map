jQuery(document).ready(function ($) {
 const map = initializeMap();

  map.on('load', () => {
    const { stop, route_overlaps } = drupalSettings.gtfs_display_map;

    addStopToMap(stop).togglePopup();

    route_overlaps.overlaps.geometries.forEach((feature, index) => {
      // if (index > 1) return;
      const id = `overlap-${index}`;
      addSegmentToMap(id, feature, feature.properties.overlap.map(routeById));
    });

    Object.keys(route_overlaps).filter(k => k !== 'overlaps').forEach(key => {
      const route = routeById(key);
      addSegmentToMap(key, route_overlaps[key], [route]);
    });
  });
});