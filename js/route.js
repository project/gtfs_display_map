jQuery(document).ready(function ($) {
  const map = initializeMap();
  const { route, stops } = drupalSettings.gtfs_display_map;
  window.stops = {};

  stops.forEach(stop => {
    window.stops[stop.stop_id] = stop;
  })

  map.on('load', () => {
    addRouteToMap(route);
    const bounds = new mapboxgl.LngLatBounds();

    addStopsToMap(stops);


    stops.forEach(function (stop) {
      bounds.extend([stop.stop_lon, stop.stop_lat]);
    });
    map.fitBounds(bounds, {
      padding: 50
    });
  });

  const uniqueRoutes = stops
      .map(stop => stop.routes)
      .flat()
      .filter((v, i, s) => s.map(r => r.route_id).indexOf(v.route_id) === i);

  createLegend(uniqueRoutes);

});